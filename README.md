# CopBird Hackathon - Ergebnisse

Hier finden sich alle Ergebnisse der Teams, die an dem Hackathon teilgenommen habe.

## Anleitung für die Teams

1. Erstellt euch einen Gitlab-Account oder benutzt einen existierenden.
2. Klont euch das Repository:
```
git clone https://gitlab.com/copbird-hackathon/ergebnisse.git
```
3. Erstellt einen neuen Branch für euer Team in der Form `team-<Teamnummer>`, also z.B. für Team 1 heißt der Branch: `team-01`.
```
git checkout -b team-<Teamnummer>
```
4. Kopiert das, was ihr an Ergebnissen veröffentlichen wollt, in den jeweiligen Teamordner z.B. für Team 1 in den Ordner `team-01`.
5. Übernehmt die Änderungen in Git (ersetzt das `<Teamnummer>` durch eure Teamnummer):
```
git add .
```
```
git commit -m "Ergebnisse Team <Teamnummer> hinzugefügt"
```
6. Ladet eure Ergebnisse auf Gitlab hoch (ersetzt das `<Teamnummer>` durch eure Teamnummer).
```
git push -u origin team-<Teamnummer>
```
7. Erstellt einen neuen Merge Request unter [Merge Requests](https://gitlab.com/copbird-hackathon/ergebnisse/-/merge_requests) von eurem Teambranch `team-<Teamnummer` auf `main`.
8. Wir schauen uns euren Merge Request an und mergen ihn in den `main`.